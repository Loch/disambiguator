# Description
This project disambiguates well known entries from a JSON file. This is a sample of the expected format:

```
{"leads":[
{
"_id": "jkj238238jdsnfsj23",
"email": "foo@bar.com",
"firstName":  "John",
"lastName": "Smith",
"address": "123 Street St",
"entryDate": "2014-05-07T17:30:20+00:00"
}
]
}
```

The elements of the 'leads' array the objects that are disambiguated.

# Running the Application
After performing a mvn install, an executable jar file with all dependencies bundled
is created:

* ./impl/target/json-disambiguator-impl-jar-with-dependencies.jar

The syntax of executing the command line application is:

* json-disambiguator-impl-jar-with-dependencies.jar -i `inputFile` -o `outputFile`

An output file is created with all of the entries disambiguated. As the entries are
read, and disambiguation is performed, the changes to the results are logged.

Entries with identity that has not been seen yet are logged as adds. The entry
is logged.

Entries that are updates to existing data are logged as updates. The old entry is
logged along with a diff that shows what the new values were changed to.

Entries that are ignored are logged as rejected. Entries are rejected because they
are less current than what has previously been encountered. The rejected entry is
logged.

# Sample Usage
```
java -jar json-disambiguator-impl-jar-with-dependencies.jar -i /path/to/leads.json -o ./disambiguatedLeads.json
```

# Requirements

* Apache Maven
* Java 1.8

