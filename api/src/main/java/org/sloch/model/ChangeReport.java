package org.sloch.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatch;
import org.sloch.datastore.LeadStore;

/**
 * <p>
 * A change report contains three things: a put result, an instance of a {@link Lead}, and a json patch.
 * It is used to provide feedback when a {@link Lead} is pushed a {@link LeadStore}.
 * </p>
 * <p>
 * The put result indicates whether the {@link Lead} that was pushed into the {@link LeadStore} was a
 * new addition, an update to an existing {@link Lead}, or was rejected. If the {@link Lead} was rejected,
 * then it means the store has a {@link Lead} with more up to date information.
 * </p>
 * <p>
 * In the case of the ADD and REJECT results, the lead is the instance that was passed into {@link LeadStore#put(Lead)}.
 * In the case of an UPDATE result, the lead is the previous lead that was in the store.
 * </p>
 * <p>
 * The the json patch is provided for ADD and UPDATE results. It is null in the case of REJECT.
 * </p>
 *
 * @author Steve Loch - sloch1981@gmail.com
 */
public class ChangeReport {

    private final PutResult putResult;
    private final Lead lead;
    private final JsonNode diff;

    public enum PutResult {
        ADD,
        UPDATE,
        REJECT
    }

    public ChangeReport(final PutResult putResult, final Lead lead, final JsonNode diff) {

        this.putResult = putResult;
        this.lead = lead;
        this.diff = diff;
    }

    public PutResult getPutResult() {
        return putResult;
    }

    public JsonNode getDiff() {
        return diff;
    }


    public String toString() {
        String jsonString;
        try {
            jsonString = new ObjectMapper().writeValueAsString(lead);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

        if(PutResult.ADD.equals(putResult)) {
            return putResult + ", New Lead: " + jsonString;

        } else if(PutResult.REJECT.equals(putResult)) {
            return putResult + ", Rejected Lead: " + jsonString;

        } else {
            return putResult + ", Old Lead: " + jsonString + ", Changes: " +diff;
        }
    }
}
