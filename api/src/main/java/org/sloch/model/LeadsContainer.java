package org.sloch.model;

import java.util.ArrayList;
import java.util.List;

/**
 * A wrapper around a list of {@link Lead}s. This class is for used to facilitate serialization and deserialization
 * to and from JSON.
 *
 * @author Steve Loch - sloch1981@gmail.com
 */
public class LeadsContainer {

    private final List<Lead> leads;


    public LeadsContainer() {
        leads = new ArrayList<Lead>();
    }

    public LeadsContainer(final List<Lead> leads) {
        this.leads = leads;
    }

    public List<Lead> getLeads() {
        return leads;
    }
}
