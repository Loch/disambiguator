package org.sloch.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Date;

/**
 * This class is a POJO that holds an element of the 'leads' json array from a leads json data file. A lead
 * has two sources of truth for its identity: the ID and EMail fields. When comparing two leads, if either
 * of the two fields are equal, then both leads are considered to have the same identity.
 *
 * @author Steve Loch - sloch1981@gmail.com
 */
@JsonPropertyOrder({ "_id", "email", "firstName", "lastName", "address", "entryDate" })
public class Lead {
    @JsonProperty("_id")
    private String id;
    private String email;
    private String firstName;
    private String lastName;
    private String address;
    private String entryDate; // Keep the entryDate as a String so that we guarantee we are preserving the timezone
                              // portion of the date string when we serialize this object.

    public String getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getAddress() {
        return address;
    }

    public String getEntryDate() {
        return entryDate;
    }
}