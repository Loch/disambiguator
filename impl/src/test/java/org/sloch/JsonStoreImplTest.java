package org.sloch;

import org.sloch.datastore.LeadStore;
import org.sloch.model.Lead;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.InputStream;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;

/**
 * These unit tests exercise the {@link LeadStore} functionality. It tests against various
 * permutations of the data to verify behavior with respect to {@link Lead} identity,
 * the 'last commit wins' and 'most recent entryDate wins' natures of {@link LeadStore}.
 *
 * @author Steve Loch - sloch1981@gmail.com
 */
public class JsonStoreImplTest {

    private final JsonDisambiguator jsonDisambiguator = new JsonDisambiguator();

    @Test
    public void subsequentLeadIsUpdate() throws Exception {
        InputStream jsonFile = JsonDisambiguator.class.getClassLoader().getResourceAsStream("subsequentLeadIsUpdate.json");
        List<Lead> leads = jsonDisambiguator.toLeads(jsonFile);
        List<Lead> results = jsonDisambiguator.disambiguate(leads);

        Assert.assertEquals(results.size(), 1, "Expected 1 lead after disambiguation");
        Assert.assertEquals(results.get(0).getFirstName(), "Sally", "Disambiguator did not choose the subsequent Lead as the one to keep");
    }

    @Test
    public void laterDateIsUpdate_1() throws Exception {
        InputStream jsonFile = JsonDisambiguator.class.getClassLoader().getResourceAsStream("laterDateIsUpdate_1.json");
        List<Lead> leads = jsonDisambiguator.toLeads(jsonFile);
        List<Lead> results = jsonDisambiguator.disambiguate(leads);

        Assert.assertEquals(results.size(), 1, "Expected 1 lead after disambiguation");
        Assert.assertEquals(results.get(0).getEntryDate(), "2014-06-07T17:30:20+00:00", "Disambiguator did not choose the correct entry based on time");
    }


    @Test
    public void laterDateIsUpdate_2() throws Exception {
        // This test also takes care of the case where a duplicate Lead is encountered later on, but the original
        // is kept because it has a more recent entry date
        InputStream jsonFile = JsonDisambiguator.class.getClassLoader().getResourceAsStream("laterDateIsUpdate_2.json");
        List<Lead> leads = jsonDisambiguator.toLeads(jsonFile);
        List<Lead> results = jsonDisambiguator.disambiguate(leads);

        Assert.assertEquals(results.size(), 1, "Expected 1 lead after disambiguation");
        Assert.assertEquals(results.get(0).getEntryDate(), "2014-06-07T17:30:20+00:00", "Disambiguator did not choose the correct entry based on time");
    }

    @Test
    public void addNewLeads() throws Exception {
        InputStream jsonFile = JsonDisambiguator.class.getClassLoader().getResourceAsStream("addNewLeads.json");
        List<Lead> leads = jsonDisambiguator.toLeads(jsonFile);
        List<Lead> results = jsonDisambiguator.disambiguate(leads);

        Assert.assertEquals(results.size(), 2, "Expected both leads to be in the results");
    }

    @Test
    public void mixedIdAndEmailIdentity_1() throws Exception {
        InputStream jsonFile = JsonDisambiguator.class.getClassLoader().getResourceAsStream("mixedIdAndEmailIdentity_1.json");
        List<Lead> leads = jsonDisambiguator.toLeads(jsonFile);
        List<Lead> results = jsonDisambiguator.disambiguate(leads);

        Assert.assertEquals(results.size(), 1, "Expected 1 lead after disambiguation");
    }

    @Test
    public void mixedIdAndEmailIdentity_2() throws Exception {
        InputStream jsonFile = JsonDisambiguator.class.getClassLoader().getResourceAsStream("mixedIdAndEmailIdentity_2.json");
        List<Lead> leads = jsonDisambiguator.toLeads(jsonFile);
        List<Lead> results = jsonDisambiguator.disambiguate(leads);

        Assert.assertEquals(results.size(), 1, "Expected 1 lead after disambiguation");
    }


    @Test
    public void testDatePattern() {
        String dateStr = "2014-05-07T17:30:20+00:00";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssxxx", Locale.ENGLISH);
        OffsetDateTime date = OffsetDateTime.parse(dateStr, formatter);
        // parse() will thow an exception if the date string doesn't match the pattern
    }

}
