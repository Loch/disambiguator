package org.sloch;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.apache.commons.cli.*;
import org.apache.commons.io.FileUtils;
import org.sloch.datastore.LeadStore;
import org.sloch.datastore.LeadStoreImpl;
import org.sloch.model.ChangeReport;
import org.sloch.model.Lead;
import org.sloch.model.LeadsContainer;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.logging.Logger;

/**
 * <p>
 * This is a command line application that disambiguates entries in a well known JSON file.
 * An output file is created which contains the disambiguated data.
 * </p>
 * <p>
 * This is a sample of the expected form of the JSON:
 * </p>
 *
 * <pre>
 *     {"leads":[
 *     {
 *     "_id": "jkj238238jdsnfsj23",
 *     "email": "foo@bar.com",
 *     "firstName":  "John",
 *     "lastName": "Smith",
 *     "address": "123 Street St",
 *     "entryDate": "2014-05-07T17:30:20+00:00"
 *     },
 *     ...
 *     ]
 *     }
 * </pre>
 *
 * @author Steve Loch - sloch1981@gmail.com
 */
public class JsonDisambiguator {
    // It wold not take much to modify this class and the LeadStore to provide generic JSON
    // disambiguation. A schema file could be provided to validate the incoming JSON. A config
    // that specifies the JSON paths to data that needs disambiguation can be used. A config
    // for the LeadStore would provide identity and disambiguation rules. And lastly the
    // LeadStore would become a generic JSON store.
    // These changes should allow for disambiguation of any well known JSON data.

    private final Logger logger = Logger.getLogger(JsonDisambiguator.class.getName());


    private static final Options cmdLineOptions = new Options()
            .addOption("i", true, "(required) path to the input JSON file")
            .addOption("o", true, "(required) path to the ouptut JSON file");

    public static void main(String[] args) throws IOException, ParseException {

        // Parse the command line options
        CommandLineParser parser = new DefaultParser();
        CommandLine cmdLine = parser.parse(cmdLineOptions, args);

        File inputFile;
        File outputFile;

        // Get the input file from the command line args
        if(cmdLine.hasOption("i")) {
            inputFile = new File(cmdLine.getOptionValue('i'));
        } else {
            new HelpFormatter().printHelp("org.sloch.JsonDisambiguator", cmdLineOptions);
            return;
        }

        // Get the output file from the command line args
        if(cmdLine.hasOption("o")) {
            outputFile = new File(cmdLine.getOptionValue('o'));
        } else {
            new HelpFormatter().printHelp("org.sloch.JsonDisambiguator", cmdLineOptions);
            return;
        }


        // Disambiguate the input json file, and output the results to a new file
        JsonDisambiguator jsonDisambiguator = new JsonDisambiguator();

        List<Lead> leads = jsonDisambiguator.toLeads(inputFile);
        List<Lead> disambiguatedLeads = jsonDisambiguator.disambiguate(leads);

        // Write the results to the output file
        LeadsContainer leadsContainer = new LeadsContainer(disambiguatedLeads);
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        FileUtils.writeStringToFile(outputFile, objectMapper.writeValueAsString(leadsContainer));
    }


    /**
     * @see #toLeads(InputStream)
     *
     * @param jsonFile a json file to disambiguation
     * @return a list of {@link Lead}s that were found in it
     * @throws IOException if there was an error reading the stream or mapping it to java classes
     */
    public List<Lead> toLeads(File jsonFile) throws IOException {
        return toLeads(new FileInputStream(jsonFile));
    }

    /**
     * Read an input stream of JSON and parse it into a list of {@link Lead}s. This is an example
     * of the expected form:
     *
     * <pre>
     *     {"leads":[
     *     {
     *     "_id": "jkj238238jdsnfsj23",
     *     "email": "foo@bar.com",
     *     "firstName":  "John",
     *     "lastName": "Smith",
     *     "address": "123 Street St",
     *     "entryDate": "2014-05-07T17:30:20+00:00"
     *     },
     *     ...
     *     ]
     *     }
     * </pre>
     * @param jsonFileStream JSON data to disambiguate
     * @return a list of {@link Lead}s that were found in it
     * @throws IOException if there was an error reading the stream or mapping it to java classes
     */
    public List<Lead> toLeads(InputStream jsonFileStream) throws IOException {

        ObjectMapper objectMapper = new ObjectMapper();
        LeadsContainer leadsContainer = objectMapper.readValue(jsonFileStream, LeadsContainer.class);
        return leadsContainer.getLeads();
    }

    /**
     * Disambiguate a list of {@link Lead}s. Identity is indicated by either the ID or the Email fields. {@link Lead}s
     * with the same identity but with a more recent entryDate value are preferred. {@link Lead}s with the same
     * identity and entryDate, but appear later in the list are preferred.
     *
     * @param leads a list of {@link Lead}s that may have duplicates
     * @return the disambiguated list of {@link Lead}s
     */
    public List<Lead> disambiguate(List<Lead> leads) {

        LeadStore leadStore = new LeadStoreImpl();

        for (Lead lead : leads) {
            ChangeReport changeReport = leadStore.put(lead);

            // Log a record of what was done. The value of 'lead' is logged for ADD and REJECT results in the
            // change report. For UPDATE results, the previous value and the changes that were applied in order
            // to update it are logged.
            logger.info(changeReport.toString());
        }

        return leadStore.getLeads();
    }
}
