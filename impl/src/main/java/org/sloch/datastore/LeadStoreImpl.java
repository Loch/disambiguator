package org.sloch.datastore;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.diff.JsonDiff;
import org.sloch.model.ChangeReport;
import org.sloch.model.Lead;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static org.sloch.model.ChangeReport.PutResult.*;

/**
 * <p>
 * An in memory implementation of {@link LeadStore}.
 * </p>
 *
 * @author Steve Loch - sloch1981@gmail.com
 */
public class LeadStoreImpl extends AbstractLeadStore {

    private final Map<String, Lead> idMap = new TreeMap<String, Lead>();
    private final Map<String, Lead> emailMap = new TreeMap<String, Lead>();

    public static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssxxx";
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(DATE_FORMAT, Locale.ENGLISH);


    @Override
    public ChangeReport put(Lead lead) {

        if( !idMap.containsKey(lead.getId()) && !emailMap.containsKey(lead.getEmail()) ) {
            // This lead is a new addition
            idMap.put(lead.getId(), lead);
            emailMap.put(lead.getEmail(), lead);

            return new ChangeReport(ADD, lead, generateDiff(new Lead(), lead)); // The diff will be all additions

        } else {
            // This lead might be an update. We need to examine the date first.
            Lead existingLead = (idMap.containsKey(lead.getId())) ? idMap.get(lead.getId()) : emailMap.get(lead.getEmail());

            // If the given lead's entry date is earlier that the existing lead, then we reject it.
            // Given leads that were entered later or have equal dates, will replace the existing one.
            if( toDate(lead.getEntryDate()).compareTo( toDate(existingLead.getEntryDate()) ) < 0) {

                // Reject the given lead. We already have one that matches the ID or email, and has a later entryDate
                return new ChangeReport(REJECT, lead, null);

            } else {
                // Update the lead.
                // First remove the relevant entries from the id and email maps, then add the new one
                Lead removedLeadById = idMap.remove(existingLead.getId());
                Lead removedLeadByEmail = emailMap.remove(existingLead.getEmail());

                if(removedLeadById == null || removedLeadByEmail == null) {
                    throw new RuntimeException("The id and email maps have become out of sync. Existing entries are not being correctly removed.");
                }

                idMap.put(lead.getId(), lead);
                emailMap.put(lead.getEmail(), lead);

                return new ChangeReport(UPDATE, existingLead, generateDiff(existingLead, lead));
            }
        }

    }


    @Override
    public List<Lead> getLeads() {
        return new ArrayList<Lead>(idMap.values());
    }


    /**
     * Helper method to convert date strings to java date objects
     *
     * @param dateString a date string of the form {@value #DATE_FORMAT}
     * @return a java date
     * @throws RuntimeException if there is a parsing error
     */
    private LocalDateTime toDate(String dateString) {
        return LocalDateTime.parse(dateString, dateTimeFormatter);
    }
}
